import {Injectable} from '@angular/core';
import {Router} from '@angular/router';
import {ToastrService} from 'ngx-toastr';

@Injectable({
    providedIn: 'root'
})
export class AppService {
    public user: any = null;

    constructor(private router: Router, private toastr: ToastrService) {}

    async loginByAuth({email, password}) {

        this.router.navigate(['/']);
    }

    async registerByAuth({email, password}) {

        this.router.navigate(['/']);
    }

    async loginByGoogle() {

        this.router.navigate(['/']);
    }

    async registerByGoogle() {
            
        this.router.navigate(['/']);
      
    }

    async loginByFacebook() {
          
        this.router.navigate(['/']);
    }

    async registerByFacebook() {
        
        this.router.navigate(['/']);
    }

    async getProfile() {
        
    }

    logout() {
        localStorage.removeItem('token');
        this.user = null;
        this.router.navigate(['/login']);
    }
}
