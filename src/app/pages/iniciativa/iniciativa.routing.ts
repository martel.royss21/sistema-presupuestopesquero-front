import {NgModule} from '@angular/core';

import {Routes, RouterModule} from '@angular/router';
import { ListadoComponent } from './listado/listado.component';
import { IniciativaComponent } from './iniciativa.component';
import { NuevoComponent } from './nuevo/nuevo.component';

const routes: Routes = [
    {
        path: '',
        component: IniciativaComponent,
        children:[
            {
                path: 'listado',
                component: ListadoComponent
            },
            {
                path: 'nuevo',
                component: NuevoComponent
            },
            {
                path: '**', 
                redirectTo: ''
            }
        ]
}];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class IniciativaRoutingModule {}
