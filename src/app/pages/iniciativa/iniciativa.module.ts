import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListadoComponent } from './listado/listado.component';
import { NuevoComponent } from './nuevo/nuevo.component';
import { IniciativaComponent } from './iniciativa.component';
import { IniciativaRoutingModule } from './iniciativa.routing';
import { ComponentsModule } from '@components/components.module';



@NgModule({
  declarations: [
    ListadoComponent,
    NuevoComponent,
    IniciativaComponent
  ],
  imports: [
    CommonModule,
    IniciativaRoutingModule,
    ComponentsModule
  ]
})
export class IniciativaModule { }
