import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListadoComponent } from './listado/listado.component';
import { ExplicacionComponent } from './explicacion/explicacion.component';
import { AvanceComponent } from './avance/avance.component';
import { DesviacionComponent } from './desviacion.component';
import { DesviacionRoutingModule } from './desviacion.routing';
import { ComponentsModule } from '@components/components.module';


@NgModule({
  declarations: [
    ListadoComponent,
    ExplicacionComponent,
    AvanceComponent,
    DesviacionComponent,
    ListadoComponent
  ],
  imports: [
    CommonModule,
    DesviacionRoutingModule,
    ComponentsModule  
  ]
})
export class DesviacionModule { }
