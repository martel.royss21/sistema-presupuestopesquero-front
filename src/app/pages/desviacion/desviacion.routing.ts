import {NgModule} from '@angular/core';

import {Routes, RouterModule} from '@angular/router';
import { DesviacionComponent } from './desviacion.component';
import { ListadoComponent } from './listado/listado.component';
import { AvanceComponent } from './avance/avance.component';
import { ExplicacionComponent } from './explicacion/explicacion.component';

const routes: Routes = [
    {
        path: '',
        component: DesviacionComponent,
        children:[
            {
                path: 'listado',
                component: ListadoComponent
            },
            {
                path: 'avance',
                component: AvanceComponent
            },
            {
                path: 'explicacion',
                component: ExplicacionComponent
            },
            {
                path: '**', 
                redirectTo: 'listado'
            }
        ]
}];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class DesviacionRoutingModule {}
