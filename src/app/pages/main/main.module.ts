import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FooterComponent } from './footer/footer.component';
import { HeaderComponent } from './header/header.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { MainComponent } from './main.component';
import { MainRoutingModule } from './main.routing';
import { UserComponent } from './header/user/user.component';
import { ComponentsModule } from '@components/components.module';

@NgModule({
  declarations: [
    FooterComponent,
    HeaderComponent,
    SidebarComponent,
    MainComponent,
    UserComponent,
  ],
  exports: [
    FooterComponent,
    HeaderComponent,
    SidebarComponent,
    MainComponent,
    UserComponent,
  ],
  imports: [
    CommonModule,
    MainRoutingModule,
    ComponentsModule,
  ]
})
export class MainModule { }
