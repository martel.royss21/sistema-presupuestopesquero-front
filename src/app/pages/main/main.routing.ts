import {NgModule} from '@angular/core';

import {Routes, RouterModule} from '@angular/router';
import {MainComponent} from '@pages/main/main.component';
import {DashboardComponent} from '@pages/dashboard/dashboard.component';
import { PrincipalComponent } from '@pages/principal/principal.component';
import { ScoreComponent } from '@pages/score/score.component';

const routes: Routes = [
    {
        path: '',
        component: MainComponent,
        children:[
          {
              path: 'desviacion',
              loadChildren: () => import('../desviacion/desviacion.module').then( m => m.DesviacionModule )
          },
          {
              path: 'iniciativa',
              loadChildren: () => import('../iniciativa/iniciativa.module').then( m => m.IniciativaModule )
          },
          {
              path: 'score',
              component: ScoreComponent
          },
          {
              path: 'dashboard',
              component: DashboardComponent
          },
          {
              path: '',
              component: PrincipalComponent
          },
          {
            path: '**', 
            redirectTo: ''
          }
        ]
}];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class MainRoutingModule {}
