import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {

  public user;
  public menu = MENU;

  constructor() {}

  ngOnInit() {
      //this.user = this.appService.user;
  }

}


export const MENU = [
    {
        name: 'Principal',
        path: ['/']
    },
    {
        name: 'Dashboard',
        path: ['/dashboard']
    },
    {
        name: 'Desviacion',
        children: [
            {
                name: 'Avance',
                path: ['/desviacion/avance']
            },
            {
                name: 'Listado',
                path: ['/desviacion/listado']
            },  
        ]
    },
    {
        name: 'Iniciativa',
        children: [
            {
                name: 'Listado',
                path: ['/iniciativa/listado']
            },
            {
                name: 'Nuevo',
                path: ['/iniciativa/nuevo']
            }
        ]
    },
    {
        name: 'Score',
        path: ['/score']
    }
];