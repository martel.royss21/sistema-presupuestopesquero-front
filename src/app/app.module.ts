import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {HttpClientModule} from '@angular/common/http';
import {AppRoutingModule} from '@/app.routing';
import {ReactiveFormsModule} from '@angular/forms';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {ToastrModule} from 'ngx-toastr';

import {AppComponent} from './app.component';
import { ScoreComponent } from './pages/score/score.component';
import { PrincipalComponent } from './pages/principal/principal.component';

import {registerLocaleData} from '@angular/common';
import localeEn from '@angular/common/locales/en';

registerLocaleData(localeEn, 'es-ES');

@NgModule({
    declarations: [
        AppComponent,
        ScoreComponent,
        PrincipalComponent
    ],
    imports: [
        BrowserModule,
        HttpClientModule,
        AppRoutingModule,
        ReactiveFormsModule,
        BrowserAnimationsModule,
        ToastrModule.forRoot({
            timeOut: 3000,
            positionClass: 'toast-top-right',
            preventDuplicates: true
        })
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule {}
